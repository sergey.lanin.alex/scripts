#!/bin/bash
set -e

file=$1
url_check () {
curl -s -o /dev/null -I -w "%{http_code}" $1
}

if test -f "$file"; then
  for item in $(cat $file)
  do
    if [[ $(url_check $item) =~ ^[4,5][0-9][0-9] ]];then
       echo $item - pizdec
       break
    fi
  echo -n "код ответа $item: "
  url_check  $item
  echo 
  done
else
  echo "Файл не существует"
fi
